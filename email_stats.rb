require 'rubygems'
require 'bundler'
require 'byebug'
require 'date'
Bundler.require(:default, :ci)

require './config'

if ARGV[0].to_i == 0
	@times = 1
else
	@times = ARGV[0].to_i
end

m = Mechanize.new
puts "Loading #{URL}"
page = m.get(URL)

# Fill out the login form
puts "Processing authentication..."
login_form = page.forms[1]
login_form['user_session[email]'] = USERNAME
login_form['user_session[password]'] = PASSWORD

# Get our new page
@mailings_page = login_form.submit
puts "Successfully loaded mailings page..."

# Create an array for our rows
@data = []

# Download the table headings
th = @mailings_page.search('table.data-table tr')[0]
local_data = []
th.search("th").each do |td|
	if td.text != '\n' or td.text != '  '
		text = td.text.gsub("\n", " ").strip
		local_data << text unless text == ""
	end
end
@data << local_data

def load_rows(mailings_page)
	mailings_page.search('table.data-table tr').each do |tr|
		local_data = []
			tr.search("td").each do |td|
				if td.text != '\n' or td.text != '  '
					text = td.text.gsub("\n", " ").strip
					if td.search('span')[0] and td.search('span')[0]['title']
						text += ' ' + td.search('span')[0]['title']
						text = DateTime.parse(text).strftime('%Y-%m-%d %I:%M%p')
					end
					local_data << text unless text == ""
				end
			end
		@data << local_data unless local_data == []
	end
end

# Download the mailing data
@times.times do |i|
	k = i + 1
	load_rows m.get('https://australianlaborparty.nationbuilder.com/admin/broadcasters/1/mailings?page='+k.to_s)
end




# Now let's export to a spreadsheet
p = Axlsx::Package.new
workbook = p.workbook

workbook.add_worksheet(:name => "Mail Stats") do |sheet|
	currency = workbook.styles.add_style :format_code => "$#,##0.00;-$#,##0.00"
	normal = workbook.styles.add_style
	date = workbook.styles.add_style :format_code => "yyyy-mm-dd hh:mm"

	sheet.add_row @data[0]

	@data.drop(1).each do |d|
		d[5] = d[5].sub("$","").gsub(",",'').to_f
		sheet.add_row d, :style => [normal, normal, normal, normal, normal, currency, normal, normal, normal, normal, normal, date]
	end

end

puts "Saving spreadsheet to #{SAVE_TO}"
p.serialize(SAVE_TO)


# Sending email

Mail.defaults do
	delivery_method :smtp, address: "smtp.mandrillapp.com",  port: 587, user_name: MANDRILL_USER, password: MANDRILL_PASS
end

message = Mail.new do
	from USERNAME
	to EMAIL_TO
	subject "NationBuilder email statistics for #{Time.now}"
	body "Please find attached latest NationBuilder statistics"
	add_file SAVE_TO
end

message.deliver!
