# NationBuilder email statistics
	This is a simple ruby script to allow for scraping of the Nation Builder mailings statistics page and saving
	to an Excel spreadsheet.

# Installation instructions

## 1. Requirements
	This script uses bundler to manage its dependencies. If you do not have bundler installed, please install
	it by running either `gem install bundler` or `sudo gem install bundler`.

## 2. Install Gems
	Once you have bundler installed run `bundle install` to install the required gems.

## 3. Configuration
	Copy the `config.rb.example` file to `config.rb` and edit the `config.rb` file to include your NationBuilder
	email address and password as well as the link to your broadcaster's mailing page.

## 4. Running
	run `ruby email_stats_ross.rb` with the number of pages you wish to pull as an integer. It will save the Excel spreadsheet to the same folder you are in.

### Until the code is updated at a later stage, further manual work will be required to get all data required ###

## 5. Converting data to match historical email data
To extract date, run =LEFT(L2,10) on first row timetamp field and drag down.
To extract time, run =MID(L2,12,5) on first row timestamp field and drag down.
To extract open rate, run =MID(D2,FIND("(",D2)+1,FIND(")",D2)-FIND("(",D2)-1) on first row 'Opened' field and drag down.
To extract click rate, run =MID(E2,FIND("(",E2)+1,FIND(")",E2)-FIND("(",E2)-1) on first row 'Clicked' field and drag down.
To convert unsubscribe rate, run =G2/C2 on first row 'Unsubs' field and drag down. Convert column to percentages

## 6. Combining data with historical email data

	1.	Copy across pulled data across into 'Email Data - Post 2013 Campaign Onwards.xlsx'
	2.	Manually pull Sender, campaign and ask fields
		### HINT: Talk to Don about this, testing will mean that there is some variation but generally all the emails will have some common data (Similar senders, same campaign, similar asks, etc)

Optionally you can add a number of pages that you want the command to scrape.

_Enjoy!_